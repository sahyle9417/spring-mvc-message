package hello.itemservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
	java 의 ResourceBundle (properties 파일)은 ISO-8859 로 인코딩 되어 있어야 하며, 이 경우 한글 깨짐
	intelliJ 에서 'Transparent native-to-ascii conversion' 을 체크해야만 IDE 에서와 실제 실행 시 모두 정상적으로 표현됨
	이 옵션은 파일을 보여 줄때는 unicode 인 것 처럼 보여주지만 ISO-8859로 저장
*/

@SpringBootApplication
public class ItemServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItemServiceApplication.class, args);
	}

/*
	// spring-boot 사용 시 아래와 같이 message 사용에 필요한 Bean 자동으로 생성해줌
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("messages", "errors");
		messageSource.setDefaultEncoding("utf-8");
		return messageSource;
	}
*/
}
